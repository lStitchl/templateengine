package com.solvegen.requestBuilder;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.*;

public class RequestBuilder {
        public Writer getRequest() throws FileNotFoundException{
        VelocityEngine engine = new VelocityEngine();
        engine.init();
        String templatePath = "/src/resourses/request.xml";
        Template template = engine.getTemplate(templatePath);
        VelocityContext context = new VelocityContext();
        context.put("name", RequestData.getName());
        context.put("age", RequestData.getAge());
        Writer writer = new StringWriter();
        template.merge(context, writer);

        return writer;
    }
}