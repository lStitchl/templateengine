package com.solvegen.requestBuilder;

/**
 * Created by Owner on 24.09.2015.
 */
public class RequestData {
    private static String name = "Josh";
    private static int age = 14;

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        RequestData.name = name;
    }

    public static int getAge() {
        return age;
    }

    public static void setAge(int age) {
        RequestData.age = age;
    }
}
